﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using D08RavillaPrateek.Models;

namespace D8Ravilla.Models
{

    public static class AppSeed
    {
        public static void Initialize(IServiceProvider serviceprovider)
        {
            var context = serviceprovider.GetService<ApplicationDbContext>();
            if (context == null) { return; }
            if (context.Movies.Any()) { return; }

            var lst = new List<Movie>()
            {
                new Movie() { title ="deadpool",ReleaseYear=2016,Runtime=180 },
                 new Movie() { title ="Spiderman",ReleaseYear=2016,Runtime=150 },
                  new Movie() { title ="Batman",ReleaseYear=2016,Runtime=160 },
            };
            context.Movies.AddRange(lst);
            context.SaveChanges();
        }
    }
}

