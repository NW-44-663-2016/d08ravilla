﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace D08RavillaPrateek.Models
{
    public class Movie
    {
        public int id { get; set; }
        public string title { get; set; }
        public int ReleaseYear { get; set; }
        public int Runtime { get; set; }
    }
}

